# a simple test script to make sure ferret is properly installed and working

require 'ferret'
require 'test/unit'
require 'fileutils'

class FerretTest < Test::Unit::TestCase

  def test_should_find_docs
    index = Ferret::Index::Index.new
    index << "some text about something"
    index << "test about other thing"
    index << "another text about the same something we said first"
    assert_equal 2, index.search('something').hits.size
  end

  def test_should_save_persistent_index
    nice_languages = %w[ Ruby Python Lua ]
    
    index = Ferret::Index::Index.new(:path => 'index.tmp')
    nice_languages.each do |lang|
      index << lang
    end
    index.commit

    index2 = Ferret::Index::Index.new(:path => 'index.tmp')
    assert_equal 0, index2.search('ruby').hits.first.doc
    assert_equal 1, index2.search('python').hits.first.doc
    assert_equal 2, index2.search('lua').hits.first.doc
  end

  def teardown
    FileUtils.rm_rf('index.tmp')
  end

end



